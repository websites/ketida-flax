const axios = require('axios')
const config = require('../src/data/releasesData.js')
const Cache = require('@11ty/eleventy-cache-assets')

module.exports = async function (eleventyConfig) {
  eleventyConfig.addGlobalData('gitlabReleases', async (collection) => {
    const response = await axios
      .get(
        `${config.releaseApi.gitlaburl}/api/v4/projects/${config.releaseApi.projectId}/releases/`,
        {
          method: 'GET',
          mode: 'cors',
          headers: process.env['RELEASETOKEN']
            ? `PRIVATE-TOKEN ${process.env['RELEASETOKEN']}`
            : '',
        }
      )
      .then((response) => {
        // console.log(response.data)
        let urlregex = /\]\(\/uploads/g
        response.data.forEach((entry) => {
          entry.description = entry.description.replace(
            urlregex,
            `](${config.releaseApi.gitlaburl}/${config.releaseApi.gitlabgroup}/${config.releaseApi.gitlabproject}/uploads`
          )
        })

        return response.data
      })
      .catch((error) => {
        console.error(error)
      })
    return response ? response : []
  })
}
