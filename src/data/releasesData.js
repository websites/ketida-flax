
const releaseApi = {
  gitlaburl: 'https://gitlab.coko.foundation', 
  projectId: '165',
  gitlabgroup: 'ketida',
  gitlabproject: 'ketida',
  token: process.env["$RELEASETOKEN"]
}

module.exports = {releaseApi}
