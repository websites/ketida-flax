---
title: "Open Source"
image: "ketida-open-source.svg"
part : 4
---

Streamlining book publishing requires developing scalable, standards-based technology solutions. Ketida turned to the Coko Foundation to build a comprehensive solution for the entire publishing community.
