---
title: "Great for Remote Teams"
image: "ketida-remote-teams.svg"
part : 3
---

Your books can be assessed from anywhere at anytime - accelerating collaboration within both remote and realspace teams. 
