---
title: "Ingest MS Word files"
image: "ketida-ingest-ms-word.svg"
part : 6
---

Upload MS Word files into Ketida automatically either one at a time or in a batch.
