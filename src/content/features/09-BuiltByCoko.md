---
title: "Built by Coko and the Coko Community"
image: "ketida-coko.svg"
part : 9
---

Ketida is built by the Coko development team. Increasingly Coko is also facilitating organisations to extend Ketida to meet their needs as well as extending Ketida on a for hire basis. The Coko end-user community of Publishers, Copy Ketida, Production Editors, Book Designers, Indexers, Authors (etc) as well as Artists, and folks from Print on Demand and Publishing Services organisations etc have all been the drivers behind the design of Ketida since the beginning.
