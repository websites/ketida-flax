---
title: "Embedded Word Processor"
image: "ketida-embedded-word.svg"
part : 5
---

Ketida has a clean, modern, web-based word processor at its core - wax. Complete with Track Changes, Math support, footnotes, comments, complex tables, Grammlerly integration and more...perfect for authors, copy editors, proofers, extensive keyboard shortcuts etc - wax feels like the word processor you always wanted...
