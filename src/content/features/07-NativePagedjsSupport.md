---
title: "Native Pagedjs support"
image: "ketida-native-pagedjs.svg"
part: 7
---

Ketida is built by the same folks that built Pagedjs - Coko. Get the best pagedmedia pagination engine you can find anywhere built natively into the best book production platform anywhere.
