---
title: "Push Button Publishing"
image: "ketida-pagedmedia-pagination.svg"
part : 2
---

Push button publishing is baked into Ketida. Typesetting happens in real-time — getting you from written word to final output as quickly as possible. Author proofs created at the push of a button, and final outputs available to you as soon as the work is completed.
