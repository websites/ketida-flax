---
title: "Alexis Georgantas (Greece)"
image: "alexis.png"
part : 12
role: Ketida Developer
---


Alexis is a full stack engineer with years of experience in applications’ development who admires the open source community and embraces collaboration. He joined Coko as a frontend engineer and works primarily on Ketida.
