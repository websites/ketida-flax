---
title: "Julien Taquet (France)"
image: "julien.jpg"
part : 10
role: Design Consultant
---

Julien is a book production expert having honed his skills with CSS book production in rapid production environments like Book Sprints. Julien now is focused on building community around Paged.js and bringing his skills to educate the growing community fo designers wanting to migrate to CSS book design.
