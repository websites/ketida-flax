---
title: "Giannis Kopanas (Greece)"
image: "giannis.jpg"
part : 13
role: Ketida Developer
---

Giannis is a full stack engineer with more than ten years of experience in Web Applications. Giannis built the xpub-collabra journal platform on PubSweet, the Wax word processor and now works with Alexis on Ketida.
