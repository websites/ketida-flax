---
title: "Barbara Rühling (Germany)"
image: "barbara.jpg"
part : 11
role: Workflow Advisor
---


Barbara has facilitated around 50 Book Sprints since 2013 to create handbooks for NGOs, universities, governments, and companies.
