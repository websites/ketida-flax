---
title: "Dione Mentis (South Africa)"
image: "dione.jpg"
part : 2
role: Ketida Technical Manager 
---

Dione is the Coko Technical Manager with a keen interest in optimising open-access production workflows and content development.
