---
title: "Karen Lauritsen (USA)"
image: "karen2021.jpg"
part : 5
role: OER Advisor
---

Karen has worked with educational communities for 20+ years, and is currently the publishing director at the Open Education Network. She is dedicated to collaborating with the collective to facilitate open knowledge sharing.
