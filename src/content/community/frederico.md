---
title: "Federico Menoyo (Argentia)"
image: "Federico.jpg"
part : 7
role: Print on Demand Advisor
---

Federico is the Project Leader for the publishing industry at Docuprint, he has 15+ years of experience providing supply chain services to global  organizations. He is currently the product owner of Livriz, a suite of services for book publishers.
