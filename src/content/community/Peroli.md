---
title: "Peroli Sivaprakasam (India)"
image: "peroli.jpg"
part : 9
role: Technology Advisor
---

Peroli is an Architect with over 20 years of experience in building SAAS applications. He has over 10 years of experience in publishing domain building web applications on submission, proofing, web-based pagination systems and now manages strategic technology implementations at Amnet Systems.
