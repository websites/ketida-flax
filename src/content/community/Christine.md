---
title: "Christine Fruin (USA)"
draft: false
image: "Christine.jpg"
part: 3
role: Publishing Advisor
---

Christine is the Scholarly Communication and Digital Initiatives Manager at Atla. Drawing upon her education and experience as an attorney, she has worked for nearly fifteen years as an academic librarian and library association professional dedicated to promoting access to and use of scholarly research and diverse special and archival collections through utilization of fair use, open access publishing, open source infrastructure, and responsible licensing.
