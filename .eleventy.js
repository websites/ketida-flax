// const cheerio = require("cheerio");

const searchFilter = require('./src/filters/searchFilter')
const Cache = require('@11ty/eleventy-cache-assets')
const pluginTOC = require('eleventy-plugin-nesting-toc')
const markdownIt = require('markdown-it')
const markdownItAnchor = require('markdown-it-anchor')
const yaml = require('js-yaml')
const slugify = require('slugify')
const gitlabRelease = require('./plugins/gitlabRelease.js')

// plugins

const addID = require('./plugins/addID.js')

module.exports = function(eleventyConfig) {
  // collection


  eleventyConfig.addPlugin(gitlabRelease)

  eleventyConfig.addFilter('slugify', function(value) {
    if (!value) {
      return ``
    }
    return slugify(value)
  })

  eleventyConfig.addPlugin(addID)

  // avoir du yaml pour la config
  eleventyConfig.addDataExtension('yaml', (contents) => yaml.load(contents))

  eleventyConfig.addCollection('sortedByOrder', function(collectionApi) {
    return collectionApi.getAll().sort((a, b) => {
      if (a.data.order > b.data.order) return 1
      else if (a.data.order < b.data.order) return -1
      else return 0
    })
  })

  eleventyConfig.addCollection('books', (collectionApi) => {
    return collectionApi
      .getFilteredByGlob('src/content/books/**/book.md')
      .sort((a, b) => {
        if (a.data.order < b.data.order) {
          return -1
        } else {
          return 1
        }
      })
  })
  eleventyConfig.addCollection('sitepage', (collectionApi) => {
    return collectionApi
      .getFilteredByGlob('src/content/site/*.*')
      .sort((a, b) => {
        if (a.data.part < b.data.part) {
          return -1
        } else {
          return 1
        }
      })
  })
  eleventyConfig.addCollection('community', (collectionApi) => {
    return collectionApi
      .getFilteredByGlob('src/content/community/*.md')
      .sort((a, b) => {
        if (a.data.part < b.data.part) {
          return -1
        } else {
          return 1
        }
      })
  })

  eleventyConfig.addCollection('features', (collectionApi) => {
    return collectionApi
      .getFilteredByGlob('src/content/features/*.md')
      .sort((a, b) => {
        if (a.data.part < b.data.part) {
          return -1
        } else {
          return 1
        }
      })
  })

  //search to create the filter

  eleventyConfig.addFilter('filterByBook', function(collection, book) {
    const filtered = collection.filter((item) => item.data.book == book)
    return filtered
  })

  // trying to put things through snowpack and not eleventy
  eleventyConfig.addPassthroughCopy({ 'static/css': '/css' })
  eleventyConfig.addPassthroughCopy({ 'static/fonts': '/fonts' })
  eleventyConfig.addPassthroughCopy({ 'static/js': '/js' })
  eleventyConfig.addPassthroughCopy({ 'static/images': '/images' })
  eleventyConfig.addPassthroughCopy({ 'static/output': '/output' })
  eleventyConfig.addPassthroughCopy({ 'static/pagedjs': '/pagedjs' })
  eleventyConfig.addPassthroughCopy({ 'static/favicon': '/' })

  // plugin TOC
  eleventyConfig.addPlugin(pluginTOC)

  let mdoptions = {
    html: true,
    linkify: true,
    typographer: true,
  }
  eleventyConfig.setLibrary(
    'md',
    markdownIt(mdoptions).use(markdownItAnchor, {})
  )

  let mdit = markdownIt(mdoptions).use(markdownItAnchor, {})

  eleventyConfig.addFilter('markdownify', function(value) {
    if (!value) {
      return `no value`
    }
    return mdit.render(value)
  })
  eleventyConfig.addFilter('markdownifyInline', function(value) {
    if (!value) {
      return `no value`
    }
    return mdit.renderInline(value)
  })
  // useful to use the toc somewhere else
  eleventyConfig.addFilter('prependLinks', function(value, prepend) {
    let regex = /<a href="/g
    return value.replace(regex, `<a href="${prepend}`)
  })

  // add latin number plugin
  eleventyConfig.addFilter('romanize', function(value) {
    return romanize(value)
  })

  eleventyConfig.addPlugin(pluginTOC, {
    tags: ['h2', 'h3', 'h4'], // which heading tags are selected headings must each have an ID attribute
    wrapper: 'nav', // element to put around the root `ol`/`ul`
    wrapperClass: 'toc', // class for the element around the root `ol`/`ul`
    ul: false, // if to use `ul` instead of `ol`
    flat: false,
  })

  // folder structures
  // -----------------------------------------------------------------------------
  // content, data and layouts comes from the src folders
  // output goes to public (for gitlab ci/cd)
  // -----------------------------------------------------------------------------
  return {
    dataTemplateEngine: 'njk',
    dir: {
      input: 'src',
      output: 'public',
      includes: 'layouts',
      data: 'data',
    },
  }
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min
}

function romanize(num) {
  // taken from Steven Levithan
  // https://blog.stevenlevithan.com/archives/javascript-roman-numeral-converter

  if (isNaN(num)) return NaN
  var digits = String(+num).split(''),
    key = [
      '',
      'C',
      'CC',
      'CCC',
      'CD',
      'D',
      'DC',
      'DCC',
      'DCCC',
      'CM',
      '',
      'X',
      'XX',
      'XXX',
      'XL',
      'L',
      'LX',
      'LXX',
      'LXXX',
      'XC',
      '',
      'I',
      'II',
      'III',
      'IV',
      'V',
      'VI',
      'VII',
      'VIII',
      'IX',
    ],
    roman = '',
    i = 3
  while (i--) roman = (key[+digits.pop() + i * 10] || '') + roman
  return Array(+digits.join('') + 1).join('M') + roman
}
